Dataset for DWML course, 2020.

We used the http://archive.ics.uci.edu/ml/datasets/Letter+Recognition dataset to create a ML model
which will try to correctly predict a letter from an image when the image data is processed
and transformed into integer values as indicated in the dataset:

All values are scaled 0-15 (Integer)

Attribute Information:

1. letter capital letter (26 values from A to Z)
2. x-box horizontal position of box (integer)
3. y-box vertical position of box (integer)
4. width width of box (integer)
5. height height of box (integer)
6. nopix total number of pixels (integer)
7. x-bar mean x of on pixels in box (integer)
8. y-bar mean y of on pixels in box (integer)
9. x2bar mean x variance (integer)
10. y2bar mean y variance (integer)
11. xybar mean x y correlation (integer)
12. x2ybr mean of x * x * y (integer)
13. xy2br mean of x * y * y (integer)
14. xedge mean edge count left to right (integer)
15. xedgecoy correlation of xedge with y (integer)
16. yedge mean edge count bottom to top (integer)
17. yedgecox correlation of yedge with x (integer)

The best result was achieved using the K-nearest neighbours algorithm aka lBk from the weka library